﻿using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transparent_Window : MonoBehaviour
{
   
#region  定義用
    [DllImport("user32.dll")]
    public static extern int MessageBox(IntPtr hwnd, string text, string caption, uint type);

    /// <summary>
    /// 設定視窗的邊緣
    /// </summary>
    private struct MARGINS
    {
        public int cxLeftWidth;
        public int cxRightWidth;
        public int cyTopHeight;
        public int cyBottomHeight;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct DWM_BLURBEHIND
    {
        public DWM_BB dwFlags;
        public bool fEnable;
        public IntPtr hRgnBlur;
        public bool fTransitionOnMaximized;
    }
    public enum DWM_BB
    {
        DWM_BB_ENABLE = 1,
        DWM_BB_BLURREGION = 2,
        DWM_BB_TRANSITIONONMAXIMIZED = 4
    }

    /// <summary>
    ///This function extends the Aero composition further into a given window, given the distances from the sides.
    /// </summary>
    [DllImport("dwmapi.dll")]
    static extern int DwmExtendFrameIntoClientArea(IntPtr hwnd, ref MARGINS margins);
    /// <summary>
    /// 抓到現在的視窗
    /// </summary>
    /// <returns></returns>
    [DllImport("user32.dll")]
    static extern IntPtr GetActiveWindow();
    /// <summary>
    /// 對於windows api中創建的視窗，可以通過SetWindowLong函數修改其樣式。
    /// </summary>
    /// <param name="hWnd"></param>
    /// <param name="nIndex">x表示視窗樣式的類別</param>
    /// <param name="dwNewLong"></param>
    /// <returns></returns>
    [DllImport("user32.dll")]
    static extern int SetWindowLong(IntPtr hWnd, int nIndex, uint dwNewLong);

    const int GWL_EXSTYLE = -20;
    /// <summary>
    /// 窗戶是分層的窗戶。如果視窗具有CS_OWNDC或CS_CLASSDC的類樣式，則無法使用此樣式。視窗 8：頂層窗戶和兒童窗戶支援WS_EX_LAYERED樣式。以前的 Windows 版本僅支援頂層視窗的WS_EX_LAYERED
    /// </summary>
    const uint WS_EX_LAYERED = 0x00080000;
    /// <summary>
    /// 窗戶不應該被畫，直到窗下的兄弟姐妹（由同一線程創建）被畫。窗戶看起來是透明的，因為底層的兄弟姐妹的窗戶已經塗上了油漆。 要在沒有這些限制的情況下實現透明度，請使用設置視窗Rgn功能。
    /// </summary>
    const uint WS_EX_TRANSPARENT = 0x00000020;

    const int GWL_STYLE = -16;
    const uint WS_POPUP = 0x80000000;
    const uint WS_VISIBLE = 0x10000000;
    /// <summary>
    /// 使視窗成爲“TopMost”類型的視窗，這種類型的視窗總是在其它視窗的前面，真到它被關閉
    /// </summary>
    static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
    /// <summary>
    /// 改變視窗的位置與狀態
    /// </summary>
    /// <param name="hWnd">哪個視窗</param>
    /// <param name="hWndInsertAfter">視窗的 Z 順序</param>
    /// <param name="x">位置X</param>
    /// <param name="y">位置Y</param>
    /// <param name="cx">大小</param>
    /// <param name="cy">大小</param>
    /// <param name="uFlags">選項</param>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint uFlags);
    
    /// <summary>
    /// 設置目標視窗大小，位置
    /// </summary>
    /// <param name="hWnd">目標視窗</param>
    /// <param name="X">目標視窗新位置X軸座標</param>
    /// <param name="Y">目標視窗新位置Y軸座標</param>
    /// <param name="nWidth">目標視窗新寬度</param>
    /// <param name="nHeight">目標視窗新高度</param>
    /// <param name="bRepaint">是否刷新新視窗</param>
    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    static extern int MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
    
    
    [DllImport("dwmapi.dll", PreserveSig = false)]
    public static extern void DwmEnableBlurBehindWindow(IntPtr hwnd, ref DWM_BLURBEHIND blurBehind);
    /// <summary>
    /// 設置視窗可穿透點擊的透明
    /// </summary>
    /// <param name="hwnd"></param>
    /// <param name="crKey">透明顏色,0為黑色 按照000000到FFFFFF顏色轉換為10進制值</param>
    /// <param name="bAlpha">透明度0-255 255全透明</param>
    /// <param name="dwFlags">透明方式，1表示將該視窗顏色為0的部分設置為透明，2表示根據透明度設置視窗的透明度</param>
    /// <returns></returns>
    [DllImport("user32", EntryPoint = "SetLayeredWindowAttributes")]
    private static extern int SetLayeredWindowAttributes(IntPtr hwnd, int crKey, int bAlpha, int dwFlags);
    [DllImport("user32.dll")]
    private static extern uint SetWindowDisplayAffinity(IntPtr hwnd, uint dwAffinity);

    /// <summary>
    /// 獲取滑鼠在螢幕上的位置
    /// </summary>
    /// <param name="lpPoint"></param>
    /// <returns></returns>
    [DllImport("user32.dll")]
    static extern bool GetCursorPos(ref POINT lpPoint);

    POINT point;
    Vector3 _clickPos;
    private IntPtr hwnd;
    Vector3 normal_up;
    Vector3 normal_CameraToPlayer;
    public struct POINT
    {
        public int X;
        public int Y;

        public POINT(int x,int y)
        {
            this.X = x;
            this.Y = y;
        }
        public override string ToString()
        {
            return ("X:"+X+",Y:"+Y);
        }
    }

    /// <summary>
    /// 場景中的主攝影機
    /// </summary>
    public GameObject _camera;
    /// <summary>
    /// 滑鼠滾輪改變值
    /// </summary>
    public float Mouse_ScrollWheel;
    /// <summary>
    /// 滑鼠X軸相對位移
    /// </summary>
    public float Mouse_X;
    /// <summary>
    /// 滑鼠Y軸相對位移
    /// </summary>
    public float Mouse_Y;

    public float MouseSpeed = 0.2f;
    [SerializeField]
    Transform Player;
    [SerializeField]
    Transform _rotateTarget;
    [SerializeField]
    float _horRotSpeed = 1f;
    private float _rotate=0.0f;
    private float _viewAngle = 0f;
    private float _verRotSpeed=1.0f;
     [SerializeField]
    [Range(30, 90)]
    float _elevationUp = 60f;
    [SerializeField]
    [Range(30, 90)]
    float _elevationDown = 60f;
    float xRotation=0.0f;
    public int screenWidth = 1920;
    public int screenHeight = 1080;
    #endregion
    // Start is called before the first frame update
    void SetWindowScreen()
    {

        Screen.SetResolution(screenWidth, screenHeight, true);
    }
    void Start()
    {

#if !UNITY_EDITOR
        
        // MessageBox(new IntPtr(0), "Hello world!", "HELLO DIALOG", 0);
        //SetWindowScreen();
        hwnd = GetActiveWindow();
        MARGINS margins = new MARGINS { cxLeftWidth = -1 };
        DwmExtendFrameIntoClientArea(hwnd, ref margins);
        ///SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED | WS_EX_TRANSPARENT);
        
        SetWindowLong(hwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);//設置視窗背景透明
        SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED);//穿透不透明

        //SetLayeredWindowAttributes(hwnd, 0, 255, 1);
        //SetWindowDisplayAffinity(hwnd, 1);

        //DWM_BLURBEHIND bb = new DWM_BLURBEHIND{  };
       
        
        //bb.dwFlags = DWM_BB.DWM_BB_ENABLE | DWM_BB.DWM_BB_TRANSITIONONMAXIMIZED | DWM_BB.DWM_BB_BLURREGION;
        //bb.hRgnBlur = IntPtr.Zero;
        //bb.fEnable = true;
        //DwmEnableBlurBehindWindow(hwnd,ref bb);

        SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, 1|2);//置頂(使用的視窗最上層)
        // Application.runInBackground = true;


#endif
    }

    // Update is called once per frame
    void Update()
    {

        SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED);//穿透不透明
        Mouse_ScrollWheel = Input.GetAxis("Mouse ScrollWheel");//滑鼠滾輪移動
        Mouse_X = Input.GetAxis("Mouse X");//滑鼠水平移動
        Mouse_Y = Input.GetAxis("Mouse Y");//滑鼠垂直移動
        

       // _camera.transform.localRotation = Quaternion.Euler(xRotation, 0.0f, 0.0f);
        
        //_camera.transform.Rotate(Vector3.up*)
        //print(" Mouse_ScrollWheel:"+Mouse_ScrollWheel);
        
        ///滑鼠左鍵按下 調整人物左右左右上下旋轉觀看
        if (Input.GetMouseButton(1))
        {
            
            _rotate = Mouse_Y;
            
            // 左右滑模型左右轉
            Player.Rotate(new Vector3(0, Mouse_X * Time.deltaTime * _horRotSpeed * -100, 0));


            normal_up=Vector3.Normalize(Player.up);
            normal_CameraToPlayer = Vector3.Normalize(_camera.transform.position - _rotateTarget.position);
            _viewAngle = Mathf.Acos(Vector3.Dot(normal_up, normal_CameraToPlayer)) *Mathf.Rad2Deg;
            if (_rotate > 0 ?
                _viewAngle < (90 + _elevationDown) :
                _viewAngle > (90 - _elevationUp))
            {
                _camera.transform.RotateAround(_rotateTarget.position, _camera.transform.right, _rotate * Time.deltaTime * _verRotSpeed * -100);
            }

            
           

        }
        ///當滑鼠滾輪移動時，移動攝影機進行縮放
        if (Mouse_ScrollWheel != 0)
        {
            _camera.transform.Translate(Vector3.forward * Mouse_ScrollWheel, Space.Self);

        }
        //當按下右鍵的那一瞬間 紀錄滑鼠座標
        if (Input.GetMouseButtonDown(2))
        {
            _clickPos = Input.mousePosition;
        }
        ///滑鼠右鍵按下
        if (Input.GetMouseButton(2))
        {
            GetCursorPos(ref point);  //獲取滑鼠在螢幕的位置

            //1.因為電腦是以螢幕左上角為0,0點，unity裡面已螢幕左下角為0,0點
            //2.減去input.mousdePosition.x是因為視窗移動的時候是以左上角為移動座標，所以要扣除滑鼠和左上角的偏移，Y也是
            //3.Y偏移後面有一個+1，是因為測試滑鼠不動情況下會出現向上移動的情況，猜測可能是Y軸是從0-200的原因
            //4.加上(int)Mouse_X是用來記錄滑鼠相對進行了多少偏移，如果不佳，那麼當滑鼠移動時，point 和mousePosition都會相對移動視窗
            //移動視窗
            //MoveWindos(hwnd,point.X-(int)Input.mousePosition.x+(int)Mouse_X*50,point.Y-(Screen.height-(int)Input.mousePosition.y)+1-(int Mouse))
            //上面程式不能實現，這段程式原理是在按下的時候記錄按下的座標，然後在滑鼠拖到的時候根據這個座標進行偏移。
            //不像上面的一樣偏移隨著滑鼠位移而改變。

            //X軸座標=滑鼠在螢幕上的x軸位置-滑鼠在UNITY視窗裡的座標 
            //y軸座標=滑鼠在螢幕上的y軸位置-(unity y軸的總高度-unity內滑鼠的y軸位置)
            //+1是因為y軸是0-299範圍，而screen.height的值為300,總會大一點
            MoveWindow(hwnd, point.X-(int)_clickPos.x, point.Y-(Screen.height-(int)_clickPos.y)+1, screenWidth, screenHeight,true);
        }
    }


}
