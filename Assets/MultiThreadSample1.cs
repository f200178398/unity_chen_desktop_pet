using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;



public class MultiThreadSample1 : MonoBehaviour
{

	bool a=false;
	[SerializeField] private bool useJobs;

	 void Update() {
		  float startTime=Time.realtimeSinceStartup;
		  

        if (useJobs)
        {
            JobHandle jobHandle = ReallyToughTaskJob();
            jobHandle.Complete();//接著main thread 繼續動作
        }
		else{
			ReallyToughTask();
		}

        Debug.Log(((Time.realtimeSinceStartup-startTime)*1000f)+"ms");
	}


	void ReallyToughTask(){
		   float value=0.0f;
		   for(int i=0;i<50000;i++){
			   value=Mathf.Exp(Mathf.Sqrt(value));
		   }
			
	}

	private JobHandle ReallyToughTaskJob(){
		ReallyToughJob job=new ReallyToughJob();
		return job.Schedule();
	}
	
}

public struct ReallyToughJob:IJob{


	public void Execute(){
		float value=0.0f;
		   for(int i=0;i<50000;i++){
			   value=Mathf.Exp(Mathf.Sqrt(value));
		   }
	}
}