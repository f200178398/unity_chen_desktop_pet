﻿using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentWindow_ray_version : MonoBehaviour

{

    #region  定義用
    [DllImport("user32.dll")]
    public static extern int MessageBox(IntPtr hwnd, string text, string caption, uint type);

    /// <summary>
    /// 設定視窗的邊緣
    /// </summary>
    private struct MARGINS
    {
        public int cxLeftWidth;
        public int cxRightWidth;
        public int cyTopHeight;
        public int cyBottomHeight;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct DWM_BLURBEHIND
    {
        public DWM_BB dwFlags;
        public bool fEnable;
        public IntPtr hRgnBlur;
        public bool fTransitionOnMaximized;
    }
    public enum DWM_BB
    {
        DWM_BB_ENABLE = 1,
        DWM_BB_BLURREGION = 2,
        DWM_BB_TRANSITIONONMAXIMIZED = 4
    }

    /// <summary>
    ///This function extends the Aero composition further into a given window, given the distances from the sides.
    /// </summary>
    [DllImport("dwmapi.dll")]
    static extern int DwmExtendFrameIntoClientArea(IntPtr hwnd, ref MARGINS margins);
    /// <summary>
    /// 抓到現在的視窗
    /// </summary>
    /// <returns></returns>
    [DllImport("user32.dll")]
    static extern IntPtr GetActiveWindow();
    /// <summary>
    /// 對於windows api中創建的視窗，可以通過SetWindowLong函數修改其樣式。
    /// </summary>
    /// <param name="hWnd"></param>
    /// <param name="nIndex">x表示視窗樣式的類別</param>
    /// <param name="dwNewLong"></param>
    /// <returns></returns>
    [DllImport("user32.dll")]
    static extern int SetWindowLong(IntPtr hWnd, int nIndex, uint dwNewLong);

    const int GWL_EXSTYLE = -20;
    /// <summary>
    /// 窗戶是分層的窗戶。如果視窗具有CS_OWNDC或CS_CLASSDC的類樣式，則無法使用此樣式。視窗 8：頂層窗戶和兒童窗戶支援WS_EX_LAYERED樣式。以前的 Windows 版本僅支援頂層視窗的WS_EX_LAYERED
    /// </summary>
    const uint WS_EX_LAYERED = 0x00080000;
    /// <summary>
    /// 窗戶不應該被畫，直到窗下的兄弟姐妹（由同一線程創建）被畫。窗戶看起來是透明的，因為底層的兄弟姐妹的窗戶已經塗上了油漆。 要在沒有這些限制的情況下實現透明度，請使用設置視窗Rgn功能。
    /// </summary>
    const uint WS_EX_TRANSPARENT = 0x00000020;

    const int GWL_STYLE = -16;
    const uint WS_POPUP = 0x80000000;
    const uint WS_VISIBLE = 0x10000000;
    /// <summary>
    /// 使視窗成爲“TopMost”類型的視窗，這種類型的視窗總是在其它視窗的前面，真到它被關閉
    /// </summary>
    static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
    /// <summary>
    /// 改變視窗的位置與狀態
    /// </summary>
    /// <param name="hWnd">哪個視窗</param>
    /// <param name="hWndInsertAfter">視窗的 Z 順序</param>
    /// <param name="x">位置X</param>
    /// <param name="y">位置Y</param>
    /// <param name="cx">大小</param>
    /// <param name="cy">大小</param>
    /// <param name="uFlags">選項</param>
    /// <returns></returns>
    [DllImport("user32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint uFlags);

    /// <summary>
    /// 設置目標視窗大小，位置
    /// </summary>
    /// <param name="hWnd">目標視窗</param>
    /// <param name="X">目標視窗新位置X軸座標</param>
    /// <param name="Y">目標視窗新位置Y軸座標</param>
    /// <param name="nWidth">目標視窗新寬度</param>
    /// <param name="nHeight">目標視窗新高度</param>
    /// <param name="bRepaint">是否刷新新視窗</param>
    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    static extern int MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);


    [DllImport("dwmapi.dll", PreserveSig = false)]
    public static extern void DwmEnableBlurBehindWindow(IntPtr hwnd, ref DWM_BLURBEHIND blurBehind);
    /// <summary>
    /// 設置視窗可穿透點擊的透明
    /// </summary>
    /// <param name="hwnd"></param>
    /// <param name="crKey">透明顏色,0為黑色 按照000000到FFFFFF顏色轉換為10進制值</param>
    /// <param name="bAlpha">透明度0-255 255全透明</param>
    /// <param name="dwFlags">透明方式，1表示將該視窗顏色為0的部分設置為透明，2表示根據透明度設置視窗的透明度</param>
    /// <returns></returns>
    [DllImport("user32", EntryPoint = "SetLayeredWindowAttributes")]
    private static extern int SetLayeredWindowAttributes(IntPtr hwnd, int crKey, int bAlpha, int dwFlags);
    [DllImport("user32.dll")]
    private static extern uint SetWindowDisplayAffinity(IntPtr hwnd, uint dwAffinity);
    [System.Runtime.InteropServices.DllImport("user32.dll", EntryPoint = "SetForegroundWindow")]
    public static extern bool SetForegroundWindow(IntPtr hWnd);//设置此窗体为活动窗体

    [DllImport("User32.dll", EntryPoint = "FindWindow", CharSet = CharSet.Auto)]
    private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
    [DllImport("user32.dll")]
    private static extern IntPtr GetForegroundWindow();
    /// <summary>
    /// 獲取滑鼠在螢幕上的位置
    /// </summary>
    /// <param name="lpPoint"></param>
    /// <returns></returns>
    [DllImport("user32.dll")]
    static extern bool GetCursorPos(ref POINT lpPoint);

    POINT point;
    Vector3 _clickPos;
    private IntPtr hwnd;
    Vector3 normal_up;
    Vector3 _CameraToPlayer;
    Vector3 normal_CameraToPlayer;
    /// <summary>
    /// 紀錄滑鼠移動位移
    /// </summary>
    Vector3 offset;
    public struct POINT
    {
        public int X;
        public int Y;

        public POINT(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        public override string ToString()
        {
            return ("X:" + X + ",Y:" + Y);
        }
    }

    /// <summary>
    /// 場景中的主攝影機
    /// </summary>
    public Camera _camera;
    //[SerializeField]
   // private Material m_Material;
    
    /// <summary>
    /// 滑鼠滾輪改變值
    /// </summary>
    public float Mouse_ScrollWheel;
    // /// <summary>
    // /// 滑鼠X軸相對位移
    // /// </summary>
    public float Mouse_X;
    // /// <summary>
    // /// 滑鼠Y軸相對位移
    // /// </summary>
    public float Mouse_Y;

    // public float MouseSpeed = 0.2f;
    [SerializeField]
    GameObject Player;
    [SerializeField]
    Transform _rotateTarget;
    [SerializeField]
    public float _horRotSpeed = 10f;
    private float _rotate = 0.0f;
    private float _viewAngle = 0f;
    public float _verRotSpeed = 10.0f;
    [SerializeField]
    [Range(30, 90)]
    float _elevationUp = 60f;
    [SerializeField]
    [Range(30, 90)]
    float _elevationDown = 60f;
    float xRotation = 0.0f;
    public int screenWidth = 700;
    public int screenHeight = 1200;
    bool isInRole = false;
    /// <summary>
    /// 設定指定layer
    /// </summary>
    private int layer_mask;

    private RaycastHit hit;
    private Ray ray;

    bool previousState=false; //儲存之前的視窗狀態
    /// <summary>
    /// 暫時存orthigraphicsize的值
    /// </summary>
    private float valTemp;
    /// <summary>
    /// 用滾輪最小到多少
    /// </summary>
    public float minSize=0.2f;
     /// <summary>
    /// 用滾輪最大到多少
    /// </summary>
    public float maxSize=5.0f;

    // RenderTexture temp;

    #endregion
    /// <summary>
    /// 設定windows大小
    /// </summary>
    void SetWindowScreen()
    {

        Screen.SetResolution(screenWidth, screenHeight, false);
    }

    void Awake()
    {

      Application.targetFrameRate = 25;
    }
    void Start()
    {
        layer_mask = LayerMask.GetMask("Player");
#if !UNITY_EDITOR
        
        
        string name = Application.productName;
		hwnd = FindWindow(null, name);
		if (hwnd == IntPtr.Zero){
            hwnd = GetForegroundWindow();
		}
		SetForegroundWindow(hwnd);

        SetWindowScreen();
        //hwnd = GetActiveWindow();
        MARGINS margins = new MARGINS { cxLeftWidth = -1 };

        SetWindowLong(hwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);//設置視窗背景透明

        DwmExtendFrameIntoClientArea(hwnd, ref margins);
        SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, 1|2);//置頂(使用的視窗最上層)
        

#endif
         
    }

    // Vector2 WindowPositionToUnity()
    // {
    //     POINT p = new POINT();
    //     GetCursorPos(ref p);
    //     return new Vector2(p.X, screenHeight - p.Y);
    // }

    // Update is called once per frame
    void Update()
    {

        //Ray ray = Camera.main.ScreenPointToRay(WindowPositionToUnity());
        //RaycastHit hitInfo;
        //if (Physics.Raycast(ray, out hitInfo, 100,layer_mask))
        //{
        //    if (hitInfo.collider.tag == "hit")
        //    {
        //        print("打到" + layer_mask);

        //    }
        //    if (!isInRole && hitInfo.collider.tag=="hit")
        //    {
        //        print("打到" + layer_mask);
        //        Debug.Log("打到");
        //        isInRole = true;
        //        SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED);
        //    }
        //}
        //else
        //{
        //    if (isInRole)
        //    {
        //        isInRole = false;
        //        SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_TRANSPARENT | WS_EX_LAYERED);
        //    }
        //}

        ///要優化的一塊
        //ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //if (Physics.Raycast(ray, out hit,100, layer_mask))  //打到layer是player的collider
        //{
        //   ///除錯用 看是否有打射線到人物
        //   // Debug.DrawLine(Camera.main.transform.position, hit.transform.position, Color.red, 0.1f, true);
        //   // Debug.Log(hit.transform.name);
        //    SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED);
        //}
        //else
        //{

        //    SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_TRANSPARENT | WS_EX_LAYERED);
        //}
        Mouse_ScrollWheel = Input.GetAxis("Mouse ScrollWheel");//滑鼠滾輪移動
        Mouse_X = Input.GetAxis("Mouse X");//滑鼠水平移動
        Mouse_Y = Input.GetAxis("Mouse Y");//滑鼠垂直移動

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        StartCoroutine(Task());
        

    }
    //void OnPreRender()
    //{
    //    temp = RenderTexture.GetTemporary(screenWidth, screenHeight);
    //    Camera.main.targetTexture = temp;
    //}
    //void OnPostRender()
    //{
    //    Camera.main.targetTexture = null; //null means framebuffer
    //    Graphics.Blit(temp, null, m_Material);
    //    RenderTexture.ReleaseTemporary(temp);
    //}

    //private void CollisionProcess(Collider current)
    //{
    //    //前有按到人物 後沒有
    //    if ((previousCollider.gameObject.layer== layer_mask) &&  (current.gameObject.layer!= layer_mask))
    //    {
    //        SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_TRANSPARENT | WS_EX_LAYERED);

    //        print("前" + previousCollider.gameObject.layer+"後"+ current.gameObject.layer);
    //    }

    //    //前沒有按到人物 後有
    //    else if ((previousCollider.gameObject.layer != layer_mask) && (current.gameObject.layer == layer_mask))
    //    {
    //        SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED);
    //        print("前" + previousCollider.gameObject.layer + "後" + current.gameObject.layer);
    //    }
    //    //不變
    //    else
    //    {
    //        print("前" + previousCollider.gameObject.layer + "後" + current.gameObject.layer);
    //        return;
    //    }
    //    // 將當前射線碰撞的物體保存為 上一個碰撞的物體
    //    previousCollider = current;


    //}

    IEnumerator Task()
    {
        bool currentState = Physics.Raycast(ray, out hit, 100, layer_mask);


        if (previousState==false&& currentState==true)  //打到layer是player的collider
        {
            //   ///除錯用 看是否有打射線到人物
            // Debug.DrawLine(Camera.main.transform.position, hit.transform.position, Color.red, 0.1f, true);
            // Debug.Log(hit.transform.name);
            //MouseEvent();
            
            
            SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED);

           // yield return null;//中斷執行後要等到下一幀同樣的 B 時間點才會接續執行
        }
        ///沒點到人物
        else if(previousState == true && currentState == false)
        {
            
        
            SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_TRANSPARENT | WS_EX_LAYERED);//可以按到後面(穿透效果)
            //yield return null;

        }
        ///不變
        else if (previousState==true&& currentState==true)
        {
               
            //按著滑鼠中鍵 左右滑模型左右轉
            MouseScrollEvent();
            /// 滑鼠滾輪放大縮小人物
            Mouse_ScrollWheel_Event();
            Mouse_Right_button();
           
        }
        ///之前有點過人物 還是可以用滾輪繼續拖動人物左右轉向
        else if (previousState==false&& currentState==false){
           // print("托啦中");

            //按著滑鼠中鍵 左右滑模型左右轉
            MouseScrollEvent();
            Mouse_Right_button();
        }
        previousState = currentState;
        yield return null;

    }

    /// <summary>
    /// 按著滑鼠中鍵 左右滑模型左右轉
    /// </summary>
    void MouseScrollEvent(){
         
         if (Input.GetMouseButton(2))
        {
            
            _rotate = Mouse_Y;
            
            // 左右滑模型左右轉
            Player.transform.Rotate(new Vector3(0, Mouse_X * Time.deltaTime * _horRotSpeed * -100, 0));


            normal_up=Vector3.Normalize(Player.transform.up);
            normal_CameraToPlayer = Vector3.Normalize(_camera.transform.position - _rotateTarget.position);
            _viewAngle = Mathf.Acos(Vector3.Dot(normal_up, normal_CameraToPlayer)) *Mathf.Rad2Deg;
            if (_rotate > 0 ?
                _viewAngle < (90 + _elevationDown) :
                _viewAngle > (90 - _elevationUp))
            {
                _camera.transform.RotateAround(_rotateTarget.position, _camera.transform.right, _rotate * Time.deltaTime * _verRotSpeed * -100);
            }

        }
    }
    
    /// <summary>
    /// 滑鼠滾輪放大縮小人物
    /// </summary>
    void Mouse_ScrollWheel_Event(){
         ///用滾輪縮小放大
            if (Mouse_ScrollWheel != 0)
            {

                // valTemp = _camera.orthographicSize;
                 valTemp += (Input.mouseScrollDelta.y / (10.0f));
                 valTemp = Mathf.Clamp(valTemp, minSize, maxSize);
                // _camera.orthographicSize = valTemp;
                Player.transform.localScale = new Vector3(transform.lossyScale.x * valTemp, transform.lossyScale.y * valTemp, transform.lossyScale.z *valTemp);
                //_camera.orthographicSize+=Input.mouseScrollDelta.y;
                //print("size:"+_camera.orthographicSize);
                /// 縮小放大 跟攝影機有關 
                //Player.transform.localScale = new Vector3(Player.transform.lossyScale.x + Input.mouseScrollDelta.y * Time.fixedDeltaTime*3, Player.transform.lossyScale.y + Input.mouseScrollDelta.y * Time.fixedDeltaTime*3, Player.transform.lossyScale.z + Input.mouseScrollDelta.y * Time.fixedDeltaTime * 3);
            }
    }
    
    /// <summary>
    /// 按右鍵拖動人物
    /// </summary>
    void Mouse_Right_button(){
        
        
         if (Input.GetMouseButtonDown(1))
        {
            // print("剛按右鍵");
            //_clickPos = Input.mousePosition;
            

            offset = Player.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Mathf.Abs(Camera.main.transform.position.z)));
            //_CameraToPlayer=_camera.transform.position-Player.transform.position;
           // print("位移"+offset);
        }
        else if(Input.GetMouseButton(1)){
           
            Player.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Mathf.Abs(Camera.main.transform.position.z))) +offset;
            //_camera.transform.position=Player.transform.position+ _CameraToPlayer;
            
            //print("按著右鍵");


        }
    }


}
